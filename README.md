[![build status](https://gitlab.com/imie-cdpn-a02/client-api/badges/master/build.svg)](https://gitlab.com/imie-cdpn-a02/client-web/commits/master)

# Client WEB

Application web de gestion des clients

## Paramètres

Création du fichier conf.php.env
Ajout d'une variable à modifier selon l'environnement de dev


## Créer un utilisateur

Création d'une page pour l'inscription.
Informations demandées :
  - email
  - mot de passe
  - numéro de téléphone
  - Adresse complète

## Page menus

Création d'une page pour les menus
  - liste des menus avec un bouton pour le détail
  - un bloc sur droite qui permet de filtrer avec des catégories de restauration

## Commande

Dans le menu, bouton panier qui permet de voir la commande
