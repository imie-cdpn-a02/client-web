//Attributes
App.Models.Restaurant = function(obj) {
	this.id       = null;
	this.name     = null;
	this.category = null;

	this.init(obj);
};
//Non-statics methods
App.Models.Restaurant.prototype = {
	init: function (obj)
	{
		for (var fld in obj) {
			if (obj.hasOwnProperty(fld)) {
				this[fld] = obj[fld];
			}
		}
	},
	save: function(callback)
	{
	},
	remove: function()
	{
	},
};
//Statics methods
App.Models.Restaurant.getAll = function(callback)
{
    var list = Array();

	$.ajax({
		url: "datas/restaurants.json",
		method: "GET",
		dataType: "json",
		async: false,
		success: function (response) {
			Object.keys(response).map(function(objectKey, index) {
				var item = response[objectKey];

				list.push(new App.Models.Menu({
					id       : item.id,
					name     : item.name,
					category : item.category,
				}));
			});

			if (typeof callback === "function") {
				callback(list);
			}
		}
	});
};
App.Models.Restaurant.getById = function(id, callback)
{
	$.ajax({
		url: "datas/restaurant/"+id+".json",
		method: "GET",
		dataType: "json",
		async: false,
		success: function (response) {
			var item = response,
				restaurant = new App.Models.Menu({
					id       : item.id,
					name     : item.name,
					category : item.category,
				});

			if (typeof callback === "function") {
				callback(restaurant);
			}
		}
	});
};
