//Attributes
App.Models.Menu = function(obj) {
	this.id          = null;
	this.restaurant  = null;
	this.name        = null;
	this.prix        = null;
	this.preparation = null;
	this.description = null;

	this.init(obj);
};
//Non-statics methods
App.Models.Menu.prototype = {
	init: function (obj)
	{
		for (var fld in obj) {
			if (obj.hasOwnProperty(fld)) {
				this[fld] = obj[fld];
			}
		}
	},
	save: function(callback)
	{
	},
	remove: function()
	{
	},
	renderItem: function()
	{
		var output = Mustache.render(App.views.item_menu, this),
			oldItem = $('#itemMenu'+this.id);

		if (oldItem.length) {
			oldItem.replaceWith(output);
		} else {
			$('.blog-list-holder').append(output);
		}
	},
	render: function()
	{
		var output = Mustache.render(App.views.menu, this),
			oldItem = $('.Causes-detail-holder');

		oldItem.replaceWith(output);
	},
};
//Statics methods
App.Models.Menu.getAll = function(callback)
{
    var list = Array();

	$.ajax({
		url: "datas/menus.json",
		method: "GET",
		async: false,
		dataType: "json",
		success: function (response) {
			Object.keys(response).map(function(objectKey, index) {
				var item = response[objectKey];

				App.Models.Restaurant.getById(item.refRestaurant, function(restaurant) {
					list.push(new App.Models.Menu({
						id          : item.refMenu,
						restaurant  : restaurant,
						name        : item.libelle,
						prix        : item.prix,
						preparation : item.preparation,
						description : item.description,
					}));
				});
			});

			if (typeof callback === "function") {
				callback(list);
			}
		}
	});
};
App.Models.Menu.getById = function(id, callback)
{
	$.ajax({
		url: "/datas/menu/"+id+".json",
		method: "GET",
		async: false,
		dataType: "json",
		success: function (response) {
			var item = response;

			App.Models.Restaurant.getById(item.refRestaurant, function(restaurant) {
				menu = new App.Models.Menu({
					id          : item.refMenu,
					restaurant  : restaurant,
					name        : item.libelle,
					prix        : item.prix,
					preparation : item.preparation,
					description : item.description,
				});
			});

			if (typeof callback === "function") {
				callback(menu);
			}
		}
	});
};
