var App = {
    // Classes
    Collections: {},
    Models: {},
    Views: {},
    Utils: {},
    // Instances & variables
    collections: {},
    contents: {},// Contenus utiles à l'application
    models: {},
    stack: [],
    vars: {
    },
    views: {},
    init: function ()
    {
        // Initialisation de l'application ici
		switch (location.pathname) {
			case '/' :
				App.views.item_menu = $('#template_item_menu').html();

				App.Models.Menu.getAll(function(list) {
					App.collections.menus = list;

					Object.keys(App.collections.menus).map(function(objectKey, index) {
						var value = App.collections.menus[objectKey];

						value.renderItem();
					});
				});

				break;
			case '/menu.html' :
				App.views.menu = $('#template_menu').html();

				App.Models.Menu.getById(App.Utils.getGetParameter('ref'), function(menu) {
					menu.render();
				});

				break;
			default:
				//code block
		}

    }
};
App.Utils.getGetParameter = function(param, url)
{
	var vars = {};

	if (! url) {
		url = window.location.href;
	}

	url.replace( 
		/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
		function(m, key, value) { // callback
			vars[key] = decodeURIComponent(value !== undefined ? value : '');
		}
	);

	if (param) {
		return vars[param] ? vars[param] : null;	
	}

	return vars;
}

$(document).ready(function()
{
    App.init();
});

