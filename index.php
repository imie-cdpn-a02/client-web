<?php
$BASE_PATH = realpath(dirname(__FILE__));

if (isset($_GET['page'])) {
	$page = $_GET['page'];
} else {
	$page = 'index';
}

if (file_exists('conf.php')) {
	include ($BASE_PATH . "/conf.php");
}

$BASE_URL = getenv('BASE_URL');

if (!$BASE_URL) {
	$BASE_URL = '';
}

if (file_exists($BASE_PATH.'/views/'.$page.'.php')) {
	include ($BASE_PATH.'/views/'.$page.'.php');
}
?>
