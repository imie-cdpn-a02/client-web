<article id="itemMenu'{{id}}" class="blog-holder tg-bottom-margin">
  <div class="row no-gutters">
    <figure class="col-lg-5 col-sm-12 blog-list-img">
      <img src="<?php echo $BASE_URL; ?>/images/blog-list/img-01.jpg" alt="img0-01">
      <div class="overlay">
        <div class="theme-border">
          <div class="tg-display-table">
            <div class="tg-display-table-cell">
              <a href="<?php echo $BASE_URL; ?>/images/blog-list/img-01.jpg" data-rel="prettyPhoto[blog-list]">+</a>
            </div>
          </div>
        </div>
      </div>
    </figure>
    <div class="col-lg-7 col-sm-12">
      <div class="post-content">
        <h3><a href="<?php echo $BASE_URL; ?>/menu.html?ref={{id}}">{{name}}</a></h3>
        <ul class="meta-post">
          <li>Restaurant : {{restaurant.name}}</li>
          <li>Catégorie : {{restaurant.category}}</li>
        </ul>
        <p>{{description}} </p>
        <a class="read-more" href="<?php echo $BASE_URL; ?>/menu.html?ref={{id}}">Read More</a>
      </div>
    </div>
  </div>
</article>
