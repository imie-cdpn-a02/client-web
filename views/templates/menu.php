<div class="Causes-detail-holder">

	<!-- Causes Img -->
	<figure class="blog-detail-img">
		<img src="<?php echo $BASE_URL; ?>/images/blog-detail.jpg" alt="Blog-detail">
		<div class="blog-overlay">
			<div class="theme-border">
				<div class="tg-display-table">
					<div class="tg-display-table-cell">
						<h3><a href="#">{{name}}</a></h3>
					</div>
				</div>
			</div>
		</div>
	</figure>
	<!-- Causes Img -->

	<!-- blog Article -->
	<div class="detail-article">
		<p>{{description}}</p>
	</div>
	<!-- Causes Article -->

	<!-- Social Tags -->
	<div class="tags-social">
		<div class="tags pull-left">
			<strong>tags:</strong>
			<ul class="tags-list-social">
				<li><a href="#">Donate</a></li>
				<li><a href="#">Sharing Love</a></li>
				<li><a href="#">Aid</a></li>
				<li><a href="#">Helping Hand</a></li>
			</ul>
		</div>
		<div class="social-tags pull-right">
			<strong>share</strong>
			<ul class="social-icons">
				<li><a class="fa fa-facebook" href="#"></a></li>
				<li><a class="fa fa-twitter" href="#"></a></li>
				<li><a class="fa fa-google-plus" href="#"></a></li>
				<li><a class="fa fa-dribbble" href="#"></a></li>
				<li><a class="fa fa-pinterest-p" href="#"></a></li>
			</ul>
		</div>
	</div>
	<!-- Social Tags -->
</div>
