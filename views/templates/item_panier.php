<div class="cart-list">
  <img src="<?php echo $BASE_URL; ?>/images/small-product-img2.jpg" alt="small-product-img2">
  <div class="cart-detail">
    <span class="product-quantity">{{quantity}} × {{name}} </span>
    <span class="final-price">{{price}} €</span>
  </div>
  <a href="#" class="pull-right cart-del-icon"><i class="fa fa-trash-o"></i></a>
</div>
