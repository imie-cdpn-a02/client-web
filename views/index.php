<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->

<html class="no-js" lang=""> <!--<![endif]-->
<?php include ($BASE_PATH . "/views/static/head.php"); ?>
<!-- INCLUDE HEAD -->
	<!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
<body>

<!-- Wrapper -->
<div class="wrapper">

	<!-- Header inner -->
	<?php include ($BASE_PATH . "/views/static/navbar.php"); ?>
	<!-- Header inner -->

	<!-- Inner banner -->
	<section class="parallax-window inerr-banner" data-image-src="<?php echo $BASE_URL; ?>/images/inner-banner/img-01.jpg" data-parallax="scroll">
		<div class="theme-border-holder">
			<div class="theme-border">
				<div class="inner-banner-heading">
					<div class="heading-holder">
						<h3>Menus</h3>
						<ul class="tg-breadcrumb">
							<li><a href="#">Allo Restooooow</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Inner banner -->

	<!-- Main Content -->
	<main id="main-contant" class="inner-main">

		<!-- Blog List -->
		<div class="container">

			<!-- Main Heading -->
			<div class="row">
				<div class="main-heading">
					<h2><span>Listes des</span>Menus</h2>
				</div>
			</div>
			<!-- Main Heading -->

			<div class="row">
				<!-- content -->
				<div class="col-lg-9 col-sm-8 blog-list-holder">
					<!-- Blog REPLACE BY AUTO INTEGRATION -->
				</div>
				<!-- content -->

				<!-- aside -->
				<aside class="col-lg-3 col-sm-4 aside">

					<!-- Search Bar -->
					<div class="aside-widget">
						<form class="aside-search">
		                    <input type="text" placeholder="Rechercher" >
		                    <a href="" class="glyphicon glyphicon-search"></a>
						</form>
					</div>
					<!-- Search Bar -->

					<!-- Categories -->
					<div class="aside-widget">
						<h5>Categories</h5>
						<div class="aside-widget-innner">
							<ul class="categories-list">
								<li><a href="#">{{category.name}}<span class="pull-right badge">{{number of menus by category}}</span></a></li>
							</ul>
						</div>
					</div>
					<!-- Categories -->

				</aside>
				<!-- aside -->
			</div>

		</div>
		<!-- Blog List -->

	</main>
	<!-- Main Content -->

	<!-- Footer -->
	<?php include ($BASE_PATH . "/views/static/footer.php"); ?>
	<!-- Footer -->

</div>
<!-- Wrapper -->
</body>
</html>
