<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang=""> <!--<![endif]-->
<?php include (realpath(dirname(__FILE__)) . "/static/head.php"); ?>
	<!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
<body>
<!-- Preloader -->

<!-- Wrapper -->
<div class="wrapper">

	<!-- Header inner -->
	<?php include (realpath(dirname(__FILE__)) . "/static/navbar.php"); ?>
	<!-- Header inner -->

	<!-- Inner banner -->
	<section class="parallax-window inerr-banner" data-image-src="<?php echo $BASE_URL; ?>/images/inner-banner/img-01.jpg" data-parallax="scroll">
		<div class="theme-border-holder">
			<div class="theme-border">
				<div class="inner-banner-heading">
					<div class="heading-holder">
						<h3>Blog Detail</h3>
						<ul class="tg-breadcrumb">
							<li><a href="#">Home</a></li>
							<li class="active">Blog Detail</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Inner banner -->

	<!-- Main Content -->
	<main id="main-contant" class="inner-main">


		<!-- Blog List -->
		<div class="container">
			<div class="row">


				<!-- Blog Detail -->
				<div class="col-lg-9 col-sm-8">
					<a href="<?php echo $BASE_URL; ?>/" style="color:black">Retour</a>
					<div class="Causes-detail-holder">
					</div>

				</div>
				<!-- Blog Detail -->

				<!-- aside -->
				<aside class="col-lg-3 col-sm-4 aside">

					<!-- Search Bar -->
					<div class="aside-widget">
						<form class="aside-search">
		                    <input type="text" placeholder="Search" >
		                    <a href="" class="glyphicon glyphicon-search"></a>
						</form>
					</div>
					<!-- Search Bar -->

					<!-- Categories -->
					<div class="aside-widget">
						<h5>Mon Panier</h5>
						<div class="aside-widget-innner">
							<ul class="categories-list">
								<li>menu 1<span class="pull-right badge">15€</span></li>
								<li>menu 2<span class="pull-right badge">25€</span></li>
								<li>Total<span class="pull-right badge">40€</span></li>
							</ul>
						</div>
					</div>
					<!-- Categories -->




				</aside>
				<!-- aside -->

			</div>
		</div>
		<!-- Blog List -->

	</main>
	<!-- Main Content -->

	<!-- Footer -->
	<?php include (realpath(dirname(__FILE__)) . "/static/footer.php"); ?>
	<!-- Footer -->

</div>
<!-- Wrapper -->

<!-- Java Script -->

</body>
</html>
