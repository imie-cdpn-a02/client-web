<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	<html class="no-js" lang=""> <!--<![endif]-->
<?php include (realpath(dirname(__FILE__)) . "/static/head.php"); ?>
	<!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
<body>

<!-- Wrapper -->
<div class="wrapper">

	<!-- Header inner -->
	<?php include (realpath(dirname(__FILE__)) . "/static/navbar.php"); ?>
	<!-- Header inner -->

	<!-- Inner banner -->
	<section class="parallax-window inerr-banner" data-image-src="../images/inner-banner/img-01.jpg" data-parallax="scroll">
		<div class="theme-border-holder">
			<div class="theme-border">
				<div class="inner-banner-heading">
					<div class="heading-holder">
						<h3>Inscription</h3>
						<ul class="tg-breadcrumb">
							<li><a href="#">Home</a></li>
							<li class="active">Inscription</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Inner banner -->

	<!-- Main Content -->
	<main id="main-contant" class="inner-main">

		<!-- Contact Colums -->
		<div class="padding-bottom">
			<div class="container">
				<div class="row">

					<!-- Main Heading -->
					<div class="col-sm-12">
						<div class="main-heading">
							<h2>Inscription</h2>
						</div>
					</div>


				</div>
			</div>
		</div>
		<!-- Contact Colums -->

		<!-- Contact Map -->
		<div class="contact-map">
			<div id="map1" class="map1"></div>
			<div class="contact-overlay">
				<div class="theme-border-holder">
					<div class="theme-border">
					</div>
				</div>
			</div>
			<div class="contact-form">
				<h3>Inscription</h3>
				<form>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Adresse mail">
						<i class="fa fa-envelope"></i>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Mot de passe">
						<i class="fa fa-unlock-alt"></i>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Numero de téléphone">
						<i class="fa fa-phone"></i>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Adresse">
						<i class="fa fa-map-marker"></i>
					</div>
					<button class="full-width-btn">Valider</button>
				</form>
			</div>
		</div>
		<!-- Contact Map -->

	</main>
	<!-- Main Content -->

	<!-- Footer -->
	<?php include (realpath(dirname(__FILE__)) . "/static/footer.php"); ?>
	<!-- Footer -->

</div>
<!-- Wrapper -->

</body>
</html>
