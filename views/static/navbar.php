<header class="header-inner">
  <div class="container">
    <div class="row">
      <strong class="inner-logo"><a href="<?php echo $BASE_URL; ?>/"><img src="<?php echo $BASE_URL; ?>/images/logo.png" alt="logo"></a></strong>
      <nav class="nav-inner">
        <div class="navbar-header">
          <a class="side-bar-btn pull-right fa fa-bars" href="#" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"></a>
          <a class="responsive-logo pull-left" href="<?php echo $BASE_URL; ?>"><img src="<?php echo $BASE_URL; ?>/images/logo.png" alt="logo"></a>
          <div class="responsive-cart-dropdown">
            <a class="cart-btn fa fa-shopping-cart" href="#" data-toggle="dropdown"><span class="shop-batch">{{count(panier)}}</span></a>
            <div class="dropdown-menu cart-dropdown">
              <div class="cart-list-holder">
                <div class="list_products_cart"></div>
                <div class="cart-del-option">
                  <a href="#" class="pull-left del"><i class="fa fa-trash-o"></i>Vider votre panier</a>
                  <span class="pull-right Subtotal">Sous-total:<strong> {{sousTotal}} €</strong></span>
                </div>
              </div>
              <div class="cart-btns">
                <a class="sm-btn pull-left" href="#">Voir le panier</a>
                <a class="sm-btn pull-right" href="#">Commander</a>
              </div>
            </div>
          </div>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul id="nav-list-inner" class="nav-list-inner">
            <li><a href="<?php echo $BASE_URL; ?>">Menus</a></li>
            <li class="dropdown hidden-xs">
              <a href="#" data-toggle="dropdown">Panier<span class="shop-batch">2</span></a>
              <div class="dropdown-menu cart-dropdown">
                <div class="cart-list-holder">
                  <div class="list_products_cart"></div>
                  <div class="cart-del-option">
                    <a href="#" class="pull-left del"><i class="fa fa-trash-o"></i>Vider votre panier</a>
                    <span class="pull-right Subtotal">Sous-total:<strong> {{sousTotal}} €</strong></span>
                  </div>
                </div>
                <div class="cart-btns">
                  <a class="sm-btn pull-left" href="#">Voir le panier</a>
                  <a class="sm-btn pull-right" href="#">Commander</a>
                </div>
              </div>
            </li>
            <li><a href="<?php echo $BASE_URL; ?>/views/contact.php">Inscription</a></li>
          </ul>

        </div>
      </nav>
    </div>
  </div>
</header>
