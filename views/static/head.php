<?php include (realpath(dirname(__FILE__)) . "/../../conf.php"); ?>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Allo Restooooow</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="apple-touch-icon" href="apple-touch-icon.png">
	<link rel="stylesheet" href="<?php echo $BASE_URL; ?>/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo $BASE_URL; ?>/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo $BASE_URL; ?>/css/style.css">
	<link rel="stylesheet" href="<?php echo $BASE_URL; ?>/css/responsive.css">
	<link rel="stylesheet" href="<?php echo $BASE_URL; ?>/css/theme-elements.css">
	<link rel="stylesheet" href="<?php echo $BASE_URL; ?>/css/color.css">
</head>
