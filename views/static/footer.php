<footer id="footer">
  <div class="container">

    <!-- Footer Columns -->
    <div class="row">

      <!-- Column -->
      <div class="col-lg-6 col-sm-6 col-xs-12">
        <div class="address-column column-widget column-widget-v2">
          <a href="#"><img src="<?php echo $BASE_URL; ?>/images/cofee-chef-cap.png" alt="cofee-chef-cap"></a>
          <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dolore laudantium, totam rem aperiam, eaque.</p>
          <ul class="address-list">
            <li><i class="fa fa-home"></i>123 Eccles Old Road, New Salford Road, East London, Uk, M6 7AF</li>
            <li><i class="fa fa-envelope-o"></i>info@domain.com</li>
            <li><i class="fa fa-phone"></i>+44 123 456 788 - 9</li>
          </ul>
        </div>
      </div>
      <!-- Column -->

      <!-- Column -->
      <div class="col-lg-6 col-sm-6 col-xs-12">
        <div class="services-column column-widget column-widget-v2">
          <h3>Service Hours</h3>
          <ul class="service-hours-list">
            <li class="close-day">
              <span class="pull-left days">Mondays</span>
              <em>..................</em>
              <span class="pull-right time">Closed</span>
            </li>
            <li>
              <strong class="pull-left days">Tues - Fri</strong>
              <em>..................</em>
              <strong class="pull-right time">09am - 12am</strong>
            </li>
            <li>
              <strong class="pull-left days">Sat - Sun</strong>
              <em>..................</em>
              <strong class="pull-right time">08am - 1am</strong>
            </li>
            <li>
              <span class="pull-left days">Public Holidays</span>
              <em>..................</em>
              <span class="pull-right time">08am - 1am</span>
            </li>
          </ul>
          <a class="xs-btn" href="#">View All</a>
        </div>
      </div>
      <!-- Column -->

    </div>
    <!-- Footer Columns -->

  </div>
  <div class="Copy-rights">
    <div class="container">
      <p class="pull-left">© 2015  |  All Rights Reserved</p>
      <ul class="footer-nav-list pull-right">
        <li><a href="<?php echo $BASE_URL; ?>">Menus</a></li>
        <li><a href="<?php echo $BASE_URL; ?>/views/contact.php">Inscription</a></li>
      </ul>
    </div>
  </div>
</footer>

<script src="<?php echo $BASE_URL; ?>/js/vendor/jquery.js"></script>
<script src="<?php echo $BASE_URL; ?>/js/vendor/bootstrap.min.js"></script>
<script src="<?php echo $BASE_URL; ?>/js/parallax.js"></script>
<script src="<?php echo $BASE_URL; ?>/js/sleekslider.js"></script>
<script src="<?php echo $BASE_URL; ?>/js/countTo.js"></script>
<script src="<?php echo $BASE_URL; ?>/js/countdown.js"></script>
<script src="<?php echo $BASE_URL; ?>/js/sticky-em.js"></script>
<script src="<?php echo $BASE_URL; ?>/js/owl-carousel.js"></script>
<script src="<?php echo $BASE_URL; ?>/js/datepicker.js"></script>
<script src="<?php echo $BASE_URL; ?>/js/clockpicker.js"></script>
<script src="<?php echo $BASE_URL; ?>/js/classie.js"></script>
<script src="<?php echo $BASE_URL; ?>/js/select-dropdown.js"></script>
<script src="<?php echo $BASE_URL; ?>/js/menu.js"></script>
<script src="<?php echo $BASE_URL; ?>/js/appear.js"></script>
<script src="<?php echo $BASE_URL; ?>/js/prettyPhoto.js"></script>
<script src="<?php echo $BASE_URL; ?>/js/isotope.pkgd.js"></script>
<script src="<?php echo $BASE_URL; ?>/js/hoverdir.js"></script>
<script src="<?php echo $BASE_URL; ?>/js/main.js"></script>

<script src="<?php echo $BASE_URL; ?>/js/mustache.min.js"></script>

<script id="template_item_menu" type="x-tmpl-mustache">
	<?php include ($BASE_PATH . "/views/templates/item_menu.php"); ?>
</script>
<script id="template_menu" type="x-tmpl-mustache">
	<?php include ($BASE_PATH . "/views/templates/menu.php"); ?>
</script>
<script src="<?php echo $BASE_URL; ?>/js/app.js"></script>
<script src="<?php echo $BASE_URL; ?>/js/models/App.Models.Restaurant.js"></script>
<script src="<?php echo $BASE_URL; ?>/js/models/App.Models.Menu.js"></script>
